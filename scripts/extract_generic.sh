#!/usr/bin/env bash

file=$1
start_marker=$2
end_marker=$3

sed -n "/$start_marker/,/$end_marker/p" $file
