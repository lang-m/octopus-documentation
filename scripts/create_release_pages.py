#!/usr/bin/env python3

# Copyright (C) 2020 Martin Lueders 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

# This Python3 module provides the functions to parse and manipulate the 
# variable descriptions in the Octopus source files. 


import sys
import glob
import json
import re
import textwrap
import getopt

releases_file = "data/releases.json"

releases_json = open(releases_file, 'r')
releases = json.load(releases_json)
releases_json.close()

for release in releases["releases"]:

    major=release["major"]

    name=release.get("name", "")

    
    output = open('default/content/Releases/Octopus-'+major+'.md','w')

    print('---', file=output)
    print('title: Octopus Version',major, file=output)
    print('weight:', 50-int(major), file=output)
    print('---', file=output)
    print('', file=output)
    if(name):
        print('Code name during development was: ', name, file=output)
        print('', file=output)
    print('{{% sub-releases "'+major.strip()+'" %}}', file=output)
    print('Installation instructions can be found {{< manual "install" "here" >}}.', file=output)
    print('### Thanks', file=output)
    print('We would like to thank everybody who has contributed to the present version of the code.', file=output)

    output.close()



