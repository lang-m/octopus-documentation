#!/usr/bin/env python3

# Copyright (C) 2020 Martin Lueders 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

# This Python3 module provides the functions to parse and manipulate the 
# variable descriptions in the Octopus source files. 


import sys
import glob
import json

releases_file = "data/releases.json"

releases_json = open(releases_file, 'r')
releases = json.load(releases_json)
releases_json.close()

tag = releases['releases'][-1]['major']+'.'+releases['releases'][-1]['minors'][-1]['number']

print(tag)