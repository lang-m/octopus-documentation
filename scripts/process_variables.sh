#!/usr/bin/env bash

rm -rf content/Variables/*

./scripts/parse_variables.py --enable-json -d data/varinfo.json --docdir=./content/ -s octopus  \
   --enable-untested --enable-undocumented 
./scripts/var2html.py --definitions=data/varinfo.json --variables='content/Variables' 
./scripts/classes.py -s octopus/src/
