#!/usr/bin/env bash

if [ ! -d public ]; then
  mkdir public
fi

if [ -d $PWD/content/ ]; then
    rm -rf $PWD/content/*
else
    mkdir $PWD/content
fi

if [ ! -d $PWD/data/ ]; then
    mkdir $PWD/data
fi

if [ ! -d $PWD/default/includes ]; then
    mkdir -p $PWD/default/includes
fi


rm -rf config_toml.end

previous_branch=`scripts/get_previous.py`
previous_name=`echo $previous_branch | cut -d"." -f 1`

stable_branch=`scripts/get_stable.py`
stable_name=`echo $stable_branch | cut -d"." -f 1`


echo '[[menu.main]]' >> config_toml.end
echo '  name = "'$previous_name'"' >> config_toml.end
echo '  branch = "'$previous_branch'"' >> config_toml.end
echo '  weight = 1' >> config_toml.end
echo '' >> config_toml.end

echo '[[menu.main]]' >> config_toml.end
echo '  name = "'$stable_name'"' >> config_toml.end
echo '  branch = "'$stable_branch'"' >> config_toml.end
echo '  weight = 2' >> config_toml.end
echo '' >> config_toml.end

echo '[[menu.main]]' >> config_toml.end
echo '  name = "main"' >> config_toml.end
echo '  branch = "main"' >> config_toml.end
echo '  weight = 3' >> config_toml.end
echo '' >> config_toml.end

scripts/create_release_pages.py
