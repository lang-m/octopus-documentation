---
title: Octopus developers team
weight: 20
---

{{% grid %}}
{{% item %}}
Current developers:
* Xavier Andrade
* Heiko Appel
* Franco Bonafé
* Alberto Castro
* Tilman Dannert
* Umberto De Giovannini
* Gabriel Gil
* Alain Delgado Gran
* Nicole Helbig
* Hannes Huebener
* Rene Jestaedt 
* Joaquim Jornet-Somoza
* Ask Horth Larsen
* Irina Lebedeva
* Martin Lueders
* Miguel A.L. Marques (hyllios)
* Fernando Nogueira
* Sebastian Ohlmann
* Micael Oliveira
* Carlo Andrea Rozzi
* Angel Rubio
* David Strubbe
* Iris Theophilou
* Alejandro Varas
* Matthieu Verstraete
* Philipp Wopperer
* Nicolas Tancogne-Dejean
{{% /item %}}

{{% item %}}
Former developers:
* Joseba Alberdi
* Fulvio Berardi
* Florian Buchholz
* Pablo García Risueño
* Johanna Fuks
* David Kammerlander
* Kevin Krieger
* Florian Lorenzen
* Danilo Nitsche
* Roberto Olivares-Amaya
* Arto Sakko
* Ravindra Shinde
* José R. F. Sousa
* Axel Thimm
* Jessica Walkenhorst
* Jan Werschnik
{{% /item %}}

{{% item %}}
{{< figure src="/images/P1280019.JPG" width="500px" caption="(former) developers of Octopus" >}}
{{% /item %}}
{{% /grid %}}