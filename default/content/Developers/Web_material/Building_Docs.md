---
Title: "Building the documentation"
Weight: 5
description: ""
---


If you are writing new material for the tutorials or the manual, it is helpful to build the documentation pages locally, before submitting the material to the {{< octopus >}} web server. All that is needed to build the documentation are {{< hugo >}} and a web server, such as [apache](http://httpd.apache.org/)

If you already have installed {{< hugo >}} and started the web server, you can continue with {{< versioned-link "developers/web_material/building_docs/#building-the-pages" "building the pages" >}}. Otherwise, follow the instructions to install {{< hugo >}} and set up a web server.

## Installing {{< hugo >}}

The documentation system requires {{< hugo >}} at least at version 0.79. {{< hugo >}} can be downloaded [here](https://github.com/gohugoio/hugo). For most operating systems, a binary version can be found [here](https://github.com/gohugoio/hugo/releases).

Once the package is installed, you can check the correct version by
```bash
hugo version
```

{{% expand "How to install the HUGO package on Debian" %}}
First, download the latest hugo package from [here](https://github.com/gohugoio/hugo/releases). At the time of writing, the latest Debian package was 
[hugo_0.89.2_Linux-64bit.deb](https://github.com/gohugoio/hugo/releases/download/v0.89.2/hugo_0.89.2_Linux-64bit.deb).
Then you can install it on your machine using:
```bash
sudo apt install ./hugo_0.89.2_Linux-64bit.deb
```
You can now check with the above {{<code "hugo version">}} command, whether it was installed correctly.
{{% /expand %}}

## Installing a web server

The easiest option is to use a docker image for the web server.

Here we assume that you already have docker on your system or know how to install it. In case, you need support, see the official documentation on
[installing docker](https://docs.docker.com/engine/install/)


### Installing the httpd docker image

Once docker is successfully installed on the system, one can simply pull a pre-defined docker image, which provides the apache2 webserver.
For this, use the command:
```bash
docker pull httpd
```
which will download the {{< code httpd >}} image.


### Starting the docker image:

We assume that {{< code "$HUGO_DOC" >}} is the directory, in which you will clone the documentation repository.

```bash
sudo docker run -dit -p 8080:80 -v "$HUGO_DOC/octopus-documentation/public":/usr/local/apache2/htdocs/ httpd
```

## Cloning the repository

The documentation framework is kept in a git repository at [gitlab](https://gitlab.com/octopus-code/octopus-documentation). 
This repository contains all necessary files (such as the shortcodes, layouts, etc.) and contains the docdock theme as a submodule.
Also the octopus code repository is a submodule of the repository. This is required as the source files as well as testfiles will be parsed while building the documentation, and also since from {{< octopus >}} version 11, all documentation source (i.e. markdown) files will be part of the {{< octopus >}} source repository.

```bash
cd $HUGO_DOC
git clone <repo-name>

cd octopus-documentation
git submodule update --init --recursive
```

## Building the pages

Building the web pages is done by the script 
```bash
./build-branch -d <Octopus_directory>
```
which performs the following steps for each selected code version (branch):

* If present, copy {{< basedir >}}/doc/html/content/* to {{< file "$HUGO_DOC/Octopus/content/" >}}
* Parse all {{< octopus >}} source files and build the {{< file "variables.json" >}} file, containing all variable information.
* Parse testsuite input files, as well as tutorial and manual files to create links to variable descriptions.
* Create the variable reference markdown files from thes above information.
* Create class diagrams.
* Run hugo to create the static pages.

If you started the {{<command "hpptd">}} docker as described above, you can access the pages under [http://localhost:8080](http://localhost:8080).
