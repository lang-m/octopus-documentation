---
title: "Videoconference autumn 2016"
weight: 15
---


This meeting took place the 22 September at 18:00 CEST.

###  Topics to discuss  

- Development and new developers.
- The git migration.
- Branches.
- Buildbot status.
- Domain migration: octopus-code.org.
- Multiple-systems support.
- Changes to the unit system.
- Status of frozen.
- Non-cubic cell basis conventions (Cartesian vs lattice vectors).
- Arranging future meetings.

###  Present  

Present: Alain, Alberto, David, Fabio, Fernando, Hannes, Heiko, Jose Rui, Micael, Umberto, Xavier

###  Meeting Minutes  

- Development and new developers:
  - We currently have a mess of people working on the code with different levels of participation.
  - We should have a developer's database: put yourself in [[Developer Directory]]
  - New developers should be announced in the octopus-devel mailing list.
  - New developers should only gain access direct access to the master and develop branches after some time. They should use pull requests in the meantime. This ensures their code is reviewed and they don't need to feel afraid to committing changes.
- Git migration:
  - We will use gitlab.com for hosting. Later we might evaluate the possibility of running our own gitlab server, but there should be a clear advantage in doing so.
  - There is already an "octopus" user on gitlab.com. Xavier reported him/her, but it might take some time until we get the name back.
  - We will use gitflow for our workflow. At the beginning, developers will be allowed to commit directly to the "develop" branch. Later everybody should use feature branches.
  - Migration will only start after all the tests pass on all the builders (see next point).
  - All branches on the repository should be tested with the buildbot.
- Branches:
  - There are several developers that have their own repositories in order to keep their developments private.
  - Private repositories are allowed in gitlab.
  - Private repositories should be forked from the main repository and hosted on gitlab. This way we won't lose those developments and can see how they are progressing.
- Buildbot status:
  - Many tests are failing because of the new scheme to generate random wave-functions. This should be fixed be reimplementing the old scheme and let the user choose between "old" and "new". 
  - Once the previous point is addressed, Micael will make a list of failing tests and assign them to developers for fixing.
- Domain migration: octopus-code.org:
  - The Apache forward and redirect configuration in www.tddft.org still needs to be done by Fernando.
- Multiple-systems support:
  - We want to modify the code to be able to model several subsystems simultaneously.
  - Changes to the parser to handle more than one system. There are several proposals about how to handle this:
    - Several input files (one per subsystem).
    - Define sections in the input file (similar to "\section{mysection}" in latex); all variable inside a section refer to a given subsystem; common variables go in their own section.
    - Use namespaces.
    - Define groups for the subsystems and use the following syntax to define the corresponding variables: group.variable = value (eg.: maxwell.spacing = 0.2)
  - Keep parallelization in mind since the start. Subsystems should be a new parallelization strategy.
  - Allow for different solutions at first and explore.
- Changes to the unit system:
  - Units in the input file are not very flexible and error prone (eg.: some variables are better expressed in units that are neither atomic nor eV/Angstrom)
  - Suggestion to remove UnitsInput mode:
    - The code itself would only deal with atomic units; all units conversion should be done by the parser.
    - All units would be put explicitly using some appropriate syntax (eg.: "Spacing = 0.2*bohr" or "Spacing = 0.2 bohr)
  - Suggestion to also remove UnitsOutput and always use the same units for output. This can be global (atomic units or eV/Angstrom for everything) or choosing the most appropriate units on a case-by-case basis.
- Status of frozen:
  - There was no time to discuss this.
- Non-cubic cell basis conventions (Cartesian vs lattice vectors):
  - Octopus is a real-space code: Cartesian coordinates are the natural choice.
  - Proposed solution: all quantities should be in Cartesian coordinates unless explicitly stated otherwise by using appropriate variable/function names.
- Arranging future meetings:
  - A better procedure should be found to agree on the location and dates of the developers' workshops.
