---
title: "Input variables"
section: "Developers"
weight: 10
---


###  Naming conventions  

Input variables should not have spaces or underscores, word separation is marked by capitalization. The options for input variables should be written in lower case and underscores to separate words.

###  Abbreviations  

This table describes the abbreviations that can be used in input variables. In order to make the variable names consistent and intuitive, only the abbreviations that appear here can be used, and they should always be used.


{{< bootstrap-table table_class="table table-hover" table_width="400px">}}
|   Word    | Abbreviation |
|-----------|--------------|
| Maximum   | Max          |
| Minimum   | Min          |
| Tolerance | Tol          |
{{< /bootstrap-table >}}

