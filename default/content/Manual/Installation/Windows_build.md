---
title: "Building Octopus on Windows"
authors: "Nicolas Tancogne-Dejean"
weight:  10
---

# Install of Octopus on Windows

In the following we look at how to install Octopus on a Windows, using Windows Subsystem for Linux (WSL).
In the following, we assume that WSL2 is already installed. Else, run as administrator in PowerShell of Command prompt the command
```bash
wsl --install
```

Note that these instruction follow closely any Linux installation, so please refer to the linux installation for more advance options

## Basic installation

In order to install Octopus, one needs first to install its dependencies. 
For a basic installation (without external libraries, no MPI nor GPU), please run first the following commands:

```bash
sudo apt install autoconf
sudo apt install libtool libtool-bin
sudo apt install g++
sudo apt install gfortran
sudo apt install make
sudo apt install libxc-dev
sudo apt install libfftw3-dev
sudo apt install libgsl-dev
sudo apt install libopenblas-dev
```

Then one can download Octopus (here {{< octopus-download >}})

```bash
wget https://octopus-code.org/download/{{< octopus-branch >}}/octopus-{{< octopus-branch >}}.tar.gz
tar -xvf octopus-{{< octopus-branch >}}.tar.gz
cd octopus-{{< octopus-branch >}}
```
and configure it and install it as usual

```bash
./configure
sudo make install
```

As usual, a prefix could also be used to select the installation directory and "make check" 
can be used to check the correctness of the installation.

