---
title: "Links"
weigth: 30
---


These are links that we find quite useful.

##  Databases  

* [CCCBDB Computational Chemistry Comparison and Benchmark Database](http://srdata.nist.gov/cccbdb/)
* [ChemWiki](http://www.ch.ic.ac.uk/wiki/index.php/Main_Page)
* [The Cambridge Cluster Database](http://www-wales.ch.cam.ac.uk/CCD.html)
* [Mineralogy Database](http://www.mindat.org/)
* [Theoretical spectral database of PAHs](http://astrochemistry.ca.infn.it/~gmalloci/)

##  Exchange-correlation functionals  

* [{{<libxc>}}](https://www.tddft.org/programs/libxc/)
* [List of functionals of Mark Casida](http://dcm.ujf-grenoble.fr/PERSONNEL/CT/casida/CompChem/DFT.html)
* [Functional Repository](http://www.cse.clrc.ac.uk/qcg/dft/)
* [Molpro list of density functionals](http://www.molpro.net/info/current/doc/manual/node189.html)
* [Marcin Dułak list of functionals](http://scsg20.unige.ch/~dulak/science.html)
* [Density Functionals from Truhlar's group](http://comp.chem.umn.edu/info/DFT.htm)

##  Other scientific codes  

* [Abinit](http://www.abinit.org)
* [GPaw](https://wiki.fysik.dtu.dk/gpaw)
* [Parsec](http://parsec.ices.utexas.edu/index.html)
* [SeqQuest](http://dft.sandia.gov/Quest/)
* [BerkeleyGW](http://www.berkeleygw.org)

##  Others  

* [Banned by Gaussian](http://www.bannedbygaussian.org/)
* [The OpenScienceProject](http://www.openscience.org/)
* [Octopus simulation tool on nanoHUB](http://nanohub.org/tools/tddft)

##  Logo  
* [[Logo | the Octopus logo]]

##  Octopuses  
* [The Cephalopods page](http://www.thecephalopodpage.org/)
* {{< article authors="Y Koutalos, T G Ebrey, H R Gilson, and B Honig" title="Octopus photoreceptor membranes. Surface charge density and pK of the Schiff base of the pigments" journal="Biophys J." volume="58(2)" pages="493–501" year="1990" url="http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=1280989" link="PubMed Central" >}}
* [Save the Pacific Northwest Tree Octopuses](http://zapatopi.net/treeoctopus/) (http://zapatopi.net/treeoctopus/treeoctopusbadge.png)
* [Video: An Octopus attacking a Shark](http://video.google.com/videoplay?docid=-7004909622962894202 )
* [Bipedal Octopuses](http://ist-socrates.berkeley.edu/~chuffard/index_files/Bipedal_octopuses.htm )
* [Mike deGruy: Hooked by an octopus](http://www.ted.com/talks/mike_degruy_hooked_by_octopus.html)
* [OCTOPUS project on animal-like robotics](http://www.octopus-project.eu)
* [The frog and the octopus: a conceptual model of software development](http://arxiv.org/abs/1209.1327)

##  In popular culture  
* The Octopus code used by mad scientist in a [music video "Stache" by Zedd](https://www.youtube.com/watch?v=lw97SWRo70I).
Look around 0:50 and 2:50. The feature to calculate isospin resonances is not available in the released version ;-)
* [A YouTube video of our octopus](https://www.youtube.com/watch?v=HBr39kZD7qg).

