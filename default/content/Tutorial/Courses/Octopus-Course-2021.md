---
Title: "Octopus course 2021"
weight: 21
---


### Octopus Training Courses in 2021:


#### Octopus Basics (Sep. 6-14)

Day 1 (6 Sep):
- {{< versioned-link "presentations/Octopus_2021.pdf" "Short presentation of the theory" >}}
- {{< versioned-link "presentations/Octopus_2021.pdf" "Key features of Octopus and numerical methods used" >}}
- Octopus basics tutorial series

Day 2 (7 Sep):
- {{< versioned-link "presentations/octopus_course_hpc_introduction.pdf" "Running Octopus in HPC systems: parallelization and GPUs" >}}

Day 3 (8 Sep):
- {{< versioned-link "presentations/octopus_optical_absorption.pdf" "Optical absorption tutorial series" >}}

Day 4 (9 Sep):
- {{< versioned-link "presentations/octopus_basics_solids.pdf" "Solids tutorial series" >}}

Day 5 (10 Sep):
- {{< versioned-link "presentations/octopus_basics_models.pdf" "Model systems" >}} 

Days 6 (13 Sep):
- Maxwell tutorials
- Free project [students choose one or more tutorials that haven't been covered yet]

Days 7 (14 Sep):
- ARPES tutorials
- Free project [students choose one or more tutorials that haven't been covered yet]

#### Octopus Advanced (Sep. 20-26)

Day 1 (20 Sep):
- {{< versioned-link "presentations/developing_octopus.pdf" "General considerations about scientific software development" >}} 
- Introduction to Octopus for developers 
- Multisystem framework 
- {{< versioned-link "presentations/oop.pdf" "OOP: concepts and Fortran implementation" >}} 

Day 2 (21 Sep):
- {{< versioned-link "presentations/octopus_code_structure.pdf" "Octopus code structure" >}} 
- Introduction to git

Day 3 (22 Sep):
- Introduction to git (cont.)
- {{< versioned-link "presentations/octopus_course_hpc_advanced.pdf" "Parallelization and performance: how to make the Octopus swim fast" >}}

Day 4 (23 Sep):
- Testing and CI: {{< versioned-link "presentations/testing.pdf" "introduction to testing, Octopus regression testsuite, buildbot" >}} and the testsuite app 
- [Debugging the code](https://octopus-code.gitlab.io/octopus-presentations/2021-09-22-debugging-octopus/)

Day 5 (24 Sep):
- Git workflows and GitLab (Micael)
- general discussions

Days 6-7 (27-28 Sep):
- Free project [students get help implementing or modifying something in the code]