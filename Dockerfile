FROM debian:11.6
RUN apt update && \
    apt install -y hugo \                             
                    git  \          
                    python-is-python3 \ 
                    imagemagick         
RUN mkdir -p /home/user/octopus

# copy all files to /home/user
COPY . /home/user/docs

# Apply patches
RUN cp /home/user/docs/docker/policy.xml /etc/ImageMagick-6/policy.xml      # ignore read/write policy for PS
# RUN cp /home/user/docs/docker/httpd.conf /usr/local/apache2/conf/httpd.conf # make apache serve from /home/user/docs/public

# build the site
RUN git config --global --add safe.directory /home/user/octopus
WORKDIR /home/user/docs
# RUN 
CMD
