export OCTOPUS_DOC=octopus/doc/html/
export HUGO_DOC=$PWD

git submodule update --init --recursive

mkdir content/
mkdir default/includes
